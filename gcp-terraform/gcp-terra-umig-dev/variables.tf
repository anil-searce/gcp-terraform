
variable "name1" {
  description = "Name of Instance"
  default = "anil-test-1-vm"
}

variable "name2" {
  description = "Name of Instance"
  default = "anil-test-2-vm"
}

variable name_prefix{
  description = "The name prefix to be applied to the instance template"
  default     = "k8s-dev"
}



variable zone {
  description = "Zone for managed instance groups."
  default = "us-east1-b"
}

variable region {
  description = "Region for cloud resources."
  default     = "asia-south1"
}

variable machine_type {
  description = "Machine type for instance."
  default     = "f1-micro"
}

variable image {
  description = "Image for instance."
  default     = "ubuntu-1604-lts"
}

variable "size" {
    description = "Instance size of instance groups"
    default = "2"
}

variable "project" {
    description = "Project Name"
    default = "anil"
}

variable "preemptible" {
  description = "Use preemptible instances - lower price but short-lived instances. See https://cloud.google.com/compute/docs/instances/preemptible for more details"
  default     = "false"
}

variable "automatic_restart" {
  description = "Automatically restart the instance if terminated by GCP - Set to false if using preemptible instances"
  default     = "true"
}

variable on_host_maintenance {
  description = "The host mainetenece."
  default     = "TERMINATE"
}

variable can_ip_forward {
  description = "Allow ip forwarding."
  default     = false
}

variable type {
  description = "The type of GPU accelerator to be added."
  default     = "nvidia-tesla-k80"
}

variable count {
  description = "The count of GPU accelerator to be added."
  default     = "0"
}

variable module_enabled {
  description = ""
  default     = true
}

variable instance_labels {
  description = "Labels added to instances."
  type        = "map"
  default     = {}
}


variable network {
  description = "Name of the network to deploy instances to."
  default     = "default"
}

variable subnetwork {
  description = "The subnetwork to deploy to"
  default     = "default"
}

variable subnetwork_project {
  description = "The project the subnetwork belongs to. If not set, var.project is used instead."
  default     = ""
}

variable access_config {
  description = "The access config block for the instances. Set to [] to remove external IP."
  type        = "list"

  default = [
    {},
  ]
}

variable network_ip {
  description = "Set the network IP of the instance in the template. Useful for instance groups of size 1."
  default     = ""
}

variable disk_auto_delete {
  description = "Whether or not the disk should be auto-deleted."
  default     = true
}

variable compute_image {
  description = "Image used for compute VMs."
  default     = "ubuntu-1804-lts"
}

variable disk_type {
  description = "The GCE disk type. Can be either pd-ssd, local-ssd, or pd-standard."
  default     = "pd-ssd"
}

variable disk_size_gb {
  description = "The size of the image in gigabytes. If not specified, it will inherit the size of its base image."
  default     = 10
}

variable target_tags {
  description = "Tag added to instances for firewall and networking."
  type        = "list"
  default     = ["allow-service"]
}

variable service_account_email {
  description = "The email of the service account for the instance template."
  default     = "default"
}

variable service_account_scopes {
  description = "List of scopes for the instance template service account"
  type        = "list"

  default = [
    "https://www.googleapis.com/auth/compute",
    "https://www.googleapis.com/auth/logging.write",
    "https://www.googleapis.com/auth/monitoring.write",
    "https://www.googleapis.com/auth/devstorage.full_control",
  ]
}

