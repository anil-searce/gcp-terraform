#Create 1st instance.

resource "google_compute_instance" "anil-test-1-vm" {
  name = "${var.name1}"
  machine_type = "${var.machine_type}"
  zone = "${var.zone}"
  project = "${var.project}"
  labels = "${var.instance_labels}"

  tags = ["${concat(list("allow-ssh"), var.target_tags)}"]

  count       = "${var.module_enabled ? 1 : 0}"

  service_account {
    email  = "${var.service_account_email}"
    scopes = ["${var.service_account_scopes}"]
  }



  boot_disk {
    auto_delete  = "${var.disk_auto_delete}"

    initialize_params = {
      type  = "PERSISTENT"
      size = "${var.disk_size_gb}"
      image = "${var.compute_image}"
      type = "${var.disk_type}"
                        }
    }

  network_interface {
    network            = "${var.subnetwork == "" ? var.network : ""}"
    subnetwork         = "${var.subnetwork}"
    access_config      = ["${var.access_config}"]
    network_ip         = "${var.network_ip}"
    subnetwork_project = "${var.subnetwork_project == "" ? var.project : var.subnetwork_project}"
  }


  can_ip_forward = "${var.can_ip_forward}"
  
  scheduling {
    preemptible         = "${var.preemptible}"
    automatic_restart   = "${var.automatic_restart}"
    on_host_maintenance = "${var.on_host_maintenance}"
  }

  guest_accelerator = {
    type  = "${var.type}"
    count = "${var.count}"
  }

  lifecycle {
    create_before_destroy = true
  }

}


#Create 2nd instance

resource "google_compute_instance" "anil-test-2-vm" {
  name         = "${var.name2}"
  machine_type = "${var.machine_type}"
  zone = "${var.zone}"
  project = "${var.project}"
  labels = "${var.instance_labels}"

  tags = ["${concat(list("allow-ssh"), var.target_tags)}"]

  count       = "${var.module_enabled ? 1 : 0}"

  service_account {
    email  = "${var.service_account_email}"
    scopes = ["${var.service_account_scopes}"]
  }

  boot_disk {
    auto_delete  = "${var.disk_auto_delete}"

    initialize_params = {
      type  = "PERSISTENT"
      size = "${var.disk_size_gb}"
      image = "${var.compute_image}"
      type = "${var.disk_type}"
                        }
    }

  network_interface {
    network            = "${var.subnetwork == "" ? var.network : ""}"
    subnetwork         = "${var.subnetwork}"
    access_config      = ["${var.access_config}"]
    network_ip         = "${var.network_ip}"
    subnetwork_project = "${var.subnetwork_project == "" ? var.project : var.subnetwork_project}"
  }


  can_ip_forward = "${var.can_ip_forward}"
  
  scheduling {
    preemptible         = "${var.preemptible}"
    automatic_restart   = "${var.automatic_restart}"
    on_host_maintenance = "${var.on_host_maintenance}"
  }

  guest_accelerator = {
    type  = "${var.type}"
    count = "${var.count}"
  }

  lifecycle {
    create_before_destroy = true
  }

}


#Create a new instance group

resource "google_compute_instance_group" "anil-test-vm-group" {
  name        = "anil-test-vm-group"
  description = "Terraform test instance group"
  zone = "${var.zone}"
  project = "${var.project}"
    
  instances = ["${google_compute_instance.anil-test-1-vm.self_link}","${google_compute_instance.anil-test-2-vm.self_link}"]

}
